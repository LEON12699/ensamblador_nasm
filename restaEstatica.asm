;Date: 2020-June-15
;autor:Israek Leon
;Tema: Resta Estática 

;do ejecutable linux:
 
;nasm -f elf64 -o restaEstatica.o restaEstatica.asm 
;ld -o restaEstatica restaEstatica.o


%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		valor de subrutina para escritura en pantalla
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro


section .data
    mensaje db "la resta de los numeros es",0
    len_mensaje equ $-mensaje
    

section .bss
    resta resb 10

section .text
    global _start

_start:

    mov eax,4           ; eax = 4
    mov ebx,2;

    sub eax,ebx
    add eax,'0'; 

    mov [resta],eax 

    print mensaje,len_mensaje ; impresion de mensaje
    print resta,10;        impresion de resultado
    

    mov eax,1;          salida dell sistema
    int 80h


