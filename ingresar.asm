;ISRAEL LEON
;INGRESO DE NUMEROS E IMPRESIÓN

;do ejecutable linux:
 
;nasm -f elf64 -o ingresar.o ingresar.asm 
;ld -o ingresar ingresar.o

%macro imprimir 2  ; definición de macro con nombre imprimir y recibe dos parámetros
	mov eax,4;		valor de subrutina para escritura en pantalla
	mov ebx,1;		estandar para salida de datos
	mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
	mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
	int 80H;		interrupcion del sitema	
%endmacro

section .data
    mensaje db 'Ingrese un número',10
    len_mensaje equ $-mensaje
    mensaje2 db "el numero ingresado es",10
    len_mensaje2 equ $-mensaje2

section .bss
    numero resb 5

section .text
    global _start

_start:
;***************imprimir mensaje**********
    imprimir mensaje,len_mensaje

    mov eax,3
    mov ebx,2
    mov ecx,numero
    mov edx,5       ; el numero de caracteres debe ser igual al definido en .bss
    int 80h

    imprimir mensaje2,len_mensaje2
    imprimir numero,5

    mov eax,1
    int 80h