;Date: 2020-June-22
;autor:Israel Leon
;Tema: Operaciones dinamicas de un solo digito 

;do ejecutable linux:
 
;nasm -f elf64 -o operaciones_dinamicas.o operaciones_dinamicas.asm 
;ld -o operaciones_dinamicas operaciones_dinamicas.o

%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro

%macro read 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,3;		valor de subrutina para escritura en pantalla
    mov ebx,2;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro

section .data

    msg_ingrese db "Ingrese numero :  " ,0
    LEN_msg_ingrese equ $-msg_ingrese

    NUMEROS db "LOS NUMEROS SON: ",0
    LEN_NUMEROS equ $-NUMEROS

    M_AND db "Y  ",0
    LEN_M_AND equ $-M_AND

    M_SUMA db "lA SUMA ES : " ,0
    LEN_M_SUMA equ $-M_SUMA
    
    M_MULTIPLICACION db "LA MULTIPLICACION ES :" ,0
    LEN_M_MULTIPLICACION equ $-M_MULTIPLICACION

    M_DIVISION db "LA DIVISON ES :" ,0
    LEN_M_DIVISION equ $-M_DIVISION

    M_RESIDUO db ", EL RESIDUO DE LA DIVISION ES : ",0 
    LEN_M_RESIDUO equ $-M_RESIDUO
    
    M_RESTA db "LA RESTA ES : ",0 
    LEN_M_RESTA equ $-M_RESTA
 
section .bss
    suma resb 5
    resta resb 5
    division resb 5
    residuo resb 5
    multiplicacion resb 5
    numero1 resb 2
    numero2 resb 2 
 
section .text
    global _start

_start: 
    
    print msg_ingrese,LEN_msg_ingrese
    read numero1,2
    print msg_ingrese,LEN_msg_ingrese
    read numero2,2

    

    print NUMEROS,LEN_NUMEROS
    print numero1,2
    print M_AND,LEN_M_AND
    print numero2,2
    
; ASCII TO NUMBERS
    sub byte [numero1],'0'
    sub byte [numero2],'0'
;****** SUMA***********+
    
    mov eax,[numero1]
    add eax,[numero2]
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [suma],eax

    
    print M_SUMA,LEN_M_SUMA
    print suma,5

;****** Resta***********+
    mov eax,[numero1]
    sub eax,[numero2]
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [resta],eax

    
    print M_RESTA,LEN_M_RESTA
    print resta,5

;*****Multiplicacion*****
    mov eax,[numero1]
    mul BYTE [numero2]
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [multiplicacion],eax

    
    print M_MULTIPLICACION,LEN_M_MULTIPLICACION
    print multiplicacion,5

;*******DIvision y residuo *********
    mov al,[numero1]
    mov bl, [numero2]
    div bl
    add al,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    add ah,'0'
    mov [division],al
    mov [residuo],ah


    print M_DIVISION,LEN_M_DIVISION
    print division,5
    print M_RESIDUO,LEN_M_RESIDUO
    print residuo,5





    
    mov eax ,1
    int 80H
