;Date: 2020-August-24
;autor:Israel Leon
;Tema: Simular Division sin instruccion div 


;do ejecutable linux:
 
;nasm -f elf64 -o LeonI.o LeonI.asm 
;ld -o LeonI LeonI.o

%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro



section .data
    MENSAJE db "Los números a dividir son 9 y 15 : " ,0
    LEN_MENSAJE equ $-MENSAJE

    mensaje3 db "DIVISION  A CERO NO ES POSIBLE" ,0
    LEN_mensaje3 equ $-mensaje3
    NW db 10
    LEN_NW equ $-NW

    MSG db "RESULTADO : " ,0
    LEN_MSG equ $-MSG

    MSG2 db "RESIDUO : " ,0
    LEN_MSG2 equ $-MSG2

section .bss
    resultado resb 2
    cociente resb 2
    
section .text
    global _start

  

_start: 
    mov eax,15  ; NUMERO A DIVIDIR
    mov ebx,9
    mov ecx,1
    
resta:
    cmp ebx,0
    jz salirNA
    cmp eax, ebx
    jl salir
    sub eax,ebx
    mov ecx,1
    add [resultado],ecx
    jmp resta

    

salir:
    add dword [resultado],48
    add eax,48
    mov [cociente],eax
    print MENSAJE,LEN_MENSAJE
    print NW,LEN_NW
    print MSG,LEN_MSG
    
    print resultado,2
    print NW,LEN_NW
    print MSG2,LEN_MSG2
    print cociente,2
    print NW,LEN_NW
    mov eax ,1
    int 80H
    

salirNA:

    print mensaje3,LEN_mensaje3




print NW,LEN_NW
    mov eax ,1
    int 80H