;Date: 2020-August-19
;autor:Israel Leon
;Tema: archivo ingreso de datos 

;do ejecutable linux:
 
;nasm -f elf64 -o archivo_ingreso.o archivo_ingreso.asm 
;ld -o archivo_ingreso archivo_ingreso.o


%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro




section .data
    MSG1 db "Ingresa datos en el archivo" ,10
    LEN_MSG1 equ $-MSG1

    archivo db "/home/israel/Projects/Kali 2020 6to dell/ensamblador NASM/Clases/archivos/archivo.txt"

section .bss
    texto resb 30   
    idarchivo resd 1
 
section .text
    global _start

leer:
    mov eax,3
    mov ebx,0
    mov ecx, texto
    mov edx,10;
    int 80H
    ret

_start: 
    mov eax,8   ; servicio para creare archivos
    mov ebx,archivo  ; direccion del archivo
    mov ecx,1 ; Modo de acceso 
                ; O-RDONLY 0; el achivo se abre oslo para leer
                ; O-WRONLY 1; EL archivo se abre para escritura
                ; O-RDWR   2: EL archivo se abre para lectura y escritura
                ; 0-CREATE 256: Crea el archivo en caso de no existir 
                ; O-APPEND 200h: El archivo se abre solo escritura al final (agregacion del elementos)
    mov edx,777h
    int 80H

    test eax,eax   ;Instrucción de comparación realiza la operación lógica &&  de dos operandos
                    ; peor NO afecta a ninguno de ellos, SOLO afecta al registro de estado.Admite
                    ; todoos los tipos de direccionamiento excepto los dos operandos en memoria
                        ;TEST REG, REG
                        ;TEST REG,MEM
                        ;TEST MEM, REG
                        ;TEST REG,INMEDIATO
                        ;TEST MEM,INMEDIATO

    jz salir
    mov dword [idarchivo],eax
    print MSG1,LEN_MSG1
        call leer

;************************+escritura en el archivo *********************
    mov eax,4;		
    mov ebx,[idarchivo];		
    mov ecx,texto;		
    mov edx,20;		
    int 80H;		






salir:
    mov eax ,1
    int 80H


