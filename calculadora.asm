;Date: 2020-July-13
;autor:Israel Leon
;Tema: Calculadora de operadores básicos 

;do ejecutable linux:
 
;nasm -f elf64 -o calculadora.o calculadora.asm 
;ld -o calculadora calculadora.o

%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
	mov eax,4;		<valor de subrutina para escritura en pantalla>
	mov ebx,1;		estandar para salida de datos
	mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
	mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
	int 80H;			interrupción del sitema	
%endmacro

%macro read 2  ; definición de macro con nombre imprimir y recibe dos parámetros
	mov eax,3;		valor de subrutina para escritura en pantalla
	mov ebx,2;		estandar para salida de datos
	mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
	mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
	int 80H;			interrupción del sitema	
%endmacro



section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1
 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3
 
	msg4		db		10,'1. Sumar',10,0
	lmsg4		equ		$ - msg4
 
	msg5		db		'2. Restar',10,0
	lmsg5		equ		$ - msg5
 
	msg6		db		'3. Multiplicar',10,0
	lmsg6		equ		$ - msg6
 
	msg7		db		'4. Dividir',10,0
	lmsg7		equ		$ - msg7
 
	
	msg8		db		10,'Seleccione la operación : ',0
	lmsg8		equ		$ - msg8
 
	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10
 
	msg11 		db 		"5.-Salir" ,0
	LEN_msg11 	equ 	$-msg11

	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
 
	; Espacios en la memoria reservados para almacenar los valores introducidos por el usuario y el resultado de la operacion.
 
	opcion:		resb 	2
	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 
section .text
 
	global _start
 
_start:
  ;******************titulo o encabezado ******
	; Imprimimos en pantalla el mensaje 1
	print msg1, lmsg1
 
	; mensaje del primer número
	print msg2, lmsg2
 
	; Obtenemos el numero 1
	read num1,2
	;****** imprimir mensaje del segundo número
	; Imprimimos en pantalla el mensaje 3
	print msg3, lmsg3
 
	; Obtenemos el numero 2
	read num2,2
	
inicio:
	; ****** menú mensajes
	; Imprimimos en pantalla el mensaje sumar
	print msg4,lmsg4
 
	; Imprimimos en pantalla el mensaje restar
	print msg5,lmsg5
	
 
	; Imprimimos en pantalla el mensaje multiplicar
	print msg6,lmsg6
	
	; Imprimimos en pantalla el mensaje dividir
	print msg7,lmsg7
	
	
	
	; Print on screen the message salir
	print msg11,LEN_msg11
	
	
	; Print on screen the message seleccionar opción
	print msg8, lmsg8
	
	; Obtenemos la opcion seleccionada por el usuario
	read opcion,2

	mov ah, [opcion]	; Movemos la opcion seleccionada a el registro AH
	sub ah, '0'		; Convertimos el valor ingresado de ascii a decimal
 


	; Comparamos el valor ingresado por el usuario para saber que operacion realizar.
	; JE = Jump if equal = Saltamos si el valor comparado es igual
 
	cmp ah, 1
	je sumar
 
	cmp ah, 2
	je restar
 
	cmp ah, 3
	je multiplicar
 
	cmp ah, 4
	je dividir

	cmp ah, 5
	je salir
 
	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	
	print msg10 ,lmsg10
	
 
	jmp inicio
 
sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	print msg9,lmsg9
	
	; Imprimimos en pantalla el resultado
	
	print resultado,2
 
	; Finalizamos el programa
	jmp inicio
 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	print msg9,lmsg9
	
	; Imprimimos en pantalla el resultado
	
	print resultado,2
 
 
	; Finalizamos el programa
	jmp inicio
 
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
; Imprimimos en pantalla el mensaje 9
	print msg9,lmsg9
	
	; Imprimimos en pantalla el resultado
	
	print resultado,2
 
	; Finalizamos el programa
	jmp inicio
 
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
; Imprimimos en pantalla el mensaje 9
	print msg9,lmsg9
	
	; Imprimimos en pantalla el resultado
	
	print resultado,2
 
	; Finalizamos el programa
	jmp inicio
 
salir:
	; Imprimimos en pantalla dos nuevas lineas
	
	print nlinea, lnlinea
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h