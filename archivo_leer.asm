;Date: 2020-August-19
;autor:Israel Leon
;Tema: archivo lectura de datos 

;do ejecutable linux:
 
;nasm -f elf64 -o archivo_leer.o archivo_leer.asm 
;ld -o archivo_leer archivo_leer.o


%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro




section .data
    MSG1 db "Leer archivos" ,10
    LEN_MSG1 equ $-MSG1

    archivo db "/home/israel/Projects/Kali 2020 6to dell/ensamblador NASM/Clases/archivos/archivo.txt"

section .bss
    texto resb 30   
    idarchivo resd 1
 
section .text
    global _start

_start: 
;*********************** abrir el archivo ***********+++
    mov eax,5   ; servicio para apertura de archivo
    mov ebx,archivo  ; direccion del archivo
    mov ecx,0 ; Modo de acceso 
                ; O-RDONLY 0; el achivo se abre oslo para leer
                ; O-WRONLY 1; EL archivo se abre para escritura
                ; O-RDWR   2: EL archivo se abre para lectura y escritura
                ; 0-CREATE 256: Crea el archivo en caso de no existir 
                ; O-APPEND 200h: El archivo se abre solo escritura al final (agregacion del elementos)
    mov edx,777h
    int 80H

    test eax,eax   ;Instrucción de comparación realiza la operación lógica &&  de dos operandos
                    ; peor NO afecta a ninguno de ellos, SOLO afecta al registro de estado.Admite
                    ; todoos los tipos de direccionamiento excepto los dos operandos en memoria
                        ;TEST REG, REG
                        ;TEST REG,MEM
                        ;TEST MEM, REG
                        ;TEST REG,INMEDIATO
                        ;TEST MEM,INMEDIATO

    jz salir
    mov dword [idarchivo],eax
    print MSG1,LEN_MSG1
; ********** leer archivo******
    mov eax,3
    mov ebx,[idarchivo]
    mov ecx, texto
    mov edx,13 ; espacio de memoria
    int 80h

    print texto,13
;*******+cerrar archivo*************

    mov eax,6
    mov ebx,[idarchivo]
    mov ecx,0
    mov edx,0
    int 80h







salir:
    mov eax ,1
    int 80H


