;Date: 2020-July-22
;autor:Israel León
;Tema: Matriz 9 x9 de asteriscos * 

;do ejecutable linux:
 
;nasm -f elf64 -o asteriscos_cuadrado.o asteriscos_cuadrado.asm 
;ld -o asteriscos_cuadrado asteriscos_cuadrado.o

%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro



section .data
    asterisco db "*" 
    

    NW db  10
    LEN_NW equ $-NW
 
section .text
    global _start

_start: 
    mov ecx,9
    mov eax,9
    

    push rax ;=9
    
imprimir_f:
    dec ecx     ; =8
    push rcx; se guarda el ocho
    print asterisco ,1 ; se imprime *
    pop rcx  ; se saca el 8
    cmp ecx,0 ; se compara !=0
    jnz imprimir_f 
    print NW,LEN_NW
    

    pop rax ; 9
    dec eax ; 8 
    push rax; se guarda 8

imprime_v:
    cmp eax,0 ; !=8
    mov ecx,9 ; ecx =9
    jnz imprimir_f 
    jz salir

    
salir:    
    mov eax ,1
    int 80H