;Date: 2020-June-17
;autor:ISAREL LEON
;Tema: OPERACIONES ESTATICAS 

;do ejecutable linux:
 
;nasm -f elf64 -o operaciones.o operaciones.asm 
;ld -o operaciones operaciones.o


%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		valor de subrutina para escritura en pantalla
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro


section .data
    M_SUMA db "lA SUMA ES : " ,0
    LEN_M_SUMA equ $-M_SUMA
    
    NL db 10,"   ",0 
    LEN_NL equ $-NL

    M_MULTIPLICACION db "LA MULTIPLICACION ES " ,0
    LEN_M_MULTIPLICACION equ $-M_MULTIPLICACION

    M_DIVISION db "LA DIVISON ES :" ,0
    LEN_M_DIVISION equ $-M_DIVISION

    M_RESIDUO db "EL RESIDUO DE LA DIVISION ES : " ,0
    LEN_M_RESIDUO equ $-M_RESIDUO
    
    M_RESTA db "LA RESTA ES : " ,0
    LEN_M_RESTA equ $-M_RESTA
section .bss
    suma resb 5
    resta resb 5
    division resb 5
    residuo resb 5
    multiplicacion resb 5
    
 
section .text
    global _start

_start: 

;*************+SUMA ********
    mov eax,4
    mov  ebx,2

    add eax,ebx
    add eax,'0';       
    mov [suma],eax

;**********-RESTA*******
    mov eax,4
    mov  ebx,2

    sub eax,ebx
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [resta],eax
;******XMULTIPLICACION*****
    mov eax,4
    mov ebx,2
    mul ebx
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [multiplicacion],eax

;*****/DIVISION*****
    mov eax,4
    mov ebx,2
    div ebx
    add al,'0';        AL= AL+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    add ah,'0';         AH= AH+48 RESIduo se guarda en ah en este caso 
   mov [division],al
   mov [residuo],ah
   
;********* impresiones******
    print NL,LEN_NL
    print M_SUMA,LEN_M_SUMA
    print suma,5
    print NL,LEN_NL
    print M_RESTA,LEN_M_RESTA
    print resta,5
    print NL,LEN_NL
    print M_MULTIPLICACION,LEN_M_MULTIPLICACION
    print multiplicacion,5
    print NL,LEN_NL
    print M_DIVISION,LEN_M_DIVISION
    print division,5
    print NL,LEN_NL
    print M_RESIDUO,LEN_M_RESIDUO
    print residuo,5
    print NL,LEN_NL
    mov eax ,1
   int 80H