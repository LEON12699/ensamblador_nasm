;Date: 2020-July-29
;autor:Israel León
;Tema: impresión triangulo invertido de asteriscos

;do ejecutable linux:
 
;nasm -f elf64 -o loop_triangulo.o loop_triangulo.asm 
;ld -o loop_triangulo loop_triangulo.o

%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro



section .data
    ASTERISCO db "*" ,0
    LEN_ASTERISCO equ $-ASTERISCO

    NW db 10
    LEN_NW equ $-NW

section .bss
    

section .text
    global _start


_start: 
    mov ecx,9

l1:
    push rcx
    ;mov rcx,9          se hace cuadrado con esta linea 
        l2: 
        push rcx
            print ASTERISCO,LEN_ASTERISCO
        pop rcx
        loop l2

    print NW,LEN_NW
    pop rcx
    loop l1

    
    mov eax ,1
    int 80H