;Date: 2020-August-03
;autor:Israel
;Tema: multiplicacion básica 

;do ejecutable linux:
 
;nasm -f elf64 -o Multiplicacion_basica.o Multiplicacion_basica.asm 
;ld -o Multiplicacion_basica Multiplicacion_basica.o
%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro



section .data
    msg1 db "*" ,0
    LEN_msg1 equ $-msg1

    msg2 db " = " ,0
    LEN_msg2 equ $-msg2

    NW db  10
    LEN_NW equ $-NW

section .bss
    a resw 2
    b resw 2
    c resw 2

section .text
    global _start

_start: 
    print NW,LEN_NW

    mov al, 3
    add al ,'0'
    mov [a],al
    mov ecx,1
    
ciclo:
    
    push rcx
    mov eax,[a]

    sub eax,'0'
    
    mul ecx

    add eax,'0'

    mov [c],eax


    add ecx,'0'
    mov [b],ecx
    
    
    
    


    print a, 2
    print msg1,LEN_msg1
    print b, 2
    print msg2,LEN_msg2
    print c, 2
    
    print NW,LEN_NW
    pop rcx
    
    inc ecx
    cmp ecx, 10
    jnz ciclo
    

    
    
salir:
    mov eax ,1
    int 80H