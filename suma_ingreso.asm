;ISRAEL LEON
;SUMA DE NUMEROS ,INGRESADOS HASTA 9

;do ejecutable linux:
 
;nasm -f elf64 -o suma_ingreso.o suma_ingreso.asm 
;ld -o suma_ingreso suma_ingreso.o

%macro imprimir 2  ; definición de macro con nombre imprimir y recibe dos parámetros
	mov eax,4;		valor de subrutina para escritura en pantalla
	mov ebx,1;		estandar para salida de datos
	mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
	mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
	int 80H;		interrupcion del sitema	
%endmacro

%macro ingresar 2
    mov eax,3;      valor de subrutina para leer ingreso teclado
    mov ebx,2;      estandar de entrada de datos
    mov ecx,%1      ; espacio en memoria para gurada de dato
    mov edx,%2       ; el numero de caracteres debe ser igual al definido en .bss
    int 80h
%endmacro

section .data
    mensaje db 'Ingrese  número',10
    len_mensaje equ $-mensaje

    mensaje2 db "Los numeros ingresados son: ",0
    len_mensaje2 equ $-mensaje2

    mensaje3 db 10,"la suma es "
    len_mensaje3 equ $-mensaje3

section .bss
    numero1 resb 5      
    numero2 resb 5
    suma resb 5

section .text
    global _start

_start:
;***************imprimir mensaje**********
    imprimir mensaje,len_mensaje    ; pide numero 1
    ingresar numero1,5
    imprimir mensaje,len_mensaje    ;pide numero 2
    ingresar numero2,5

    

    imprimir mensaje2,len_mensaje2; 
    imprimir numero1,5;         impresión de número
    imprimir numero2,5;         impresión de número

;******* 1era forma*****
;   sub dword [numero1],'0' ;dword sirve para especificar tamaño de 32 bits en especificacion de memoria asignación directa 
;   sub dword [numero2],'0'
;    mov eax,[numero1]
;    add eax,[numero2]
;    add eax,'0'
;    mov[suma],eax

;*********2da forma********
    mov eax, [numero1]          ; eax= numero1
    sub eax,'0'                 ; se transforma a número el valor ingresado, cadena en ascci se vuelve número
    add eax,[numero2]           ; eax=eax+numero2 , se deja en formato ascii no se transforma a numero para imprimirlo
    mov [suma],eax              ;suma =eax


;****** impresion de resultado*****
    imprimir mensaje3,len_mensaje3
    imprimir suma,5
;******salida del sistema******
    mov eax,1
    int 80h