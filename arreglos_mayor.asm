;Date: 2020-August-19
;autor:Israel Leon
;Tema: arreglos_ 

;do ejecutable linux:
 
;nasm -f elf64 -o arreglos.o arreglos.asm 
;ld -o arreglos arreglos.o



section .data
    MSG1 db "Ingrese 5 números" ,10
    LEN_MSG1 equ $-MSG1

    MSG2 db "Número mayor es:" ,10
    LEN_MSG2 equ $-MSG2

    arreglo db 0,0,0,0,0 
    len_arreglo equ $-arreglo

section .bss
    numero resb 2
 
section .text
    global _start

_start: 
    
    mov esi,arreglo
    mov edi,0

    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,MSG1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,LEN_MSG1;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
    
leer:
    mov eax,3;		valor de subrutina para escritura en pantalla
    mov ebx,2;		estandar para salida de datos
    mov ecx,numero;		primer valor en el macro es el mensaje que  
    mov edx,2;		el segundo valor que define la longitud  
    int 80H;			interrupción del sitema	
    
    mov al,[numero] ; se mueve numero ingresado al registro
    sub al,'0'      ; se quita la cadena

    mov [esi],al;   movemos el valor a un indice del arreglo

    inc edi,    ; incremenetamos en uno
    inc esi     ; incrementamos en uno
    
    cmp edi,len_arreglo
    jb leer         ; se activa cuando el primer operando en menor al segundo

    mov ecx,0
    mov bl,0

comparacion:
    
    mov al,[arreglo+ecx]
    cmp al,bl
    jb contador
    mov bl,al 

contador:
    inc ecx
    cmp ecx, len_arreglo
    jb comparacion

imprimir:
    add bl,'0'
    mov [numero],bl

    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,MSG2;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,LEN_MSG2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	

    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,numero;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	

salir:
    mov eax ,1
    int 80H