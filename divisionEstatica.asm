;Date: 2020-June-15
;autor:Israek Leon
;Tema: Resta Estática 

;do ejecutable linux:
 
;nasm -f elf64 -o restaEstatica.o restaEstatica.asm 
;ld -o restaEstatica restaEstatica.o


%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		valor de subrutina para escritura en pantalla
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro


section .data
    mensaje db "la division de los numeros es",0
    len_mensaje equ $-mensaje
    

section .bss
    division resb 5

section .text
    global _start

_start:

    
    mov al, 6
    mov bl,3
    div bl

    add ax,'0'


    mov [division],ax

    print mensaje,len_mensaje ; impresion de mensaje
    print division,5;        impresion de resultado
    

    mov eax,1;          salida dell sistema
    int 80h


