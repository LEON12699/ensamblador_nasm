;Date: 2020-August-12
;autor:Israel Leon
;Tema: arreglos impresión

;do ejecutable linux:

;nasm -f elf64 -o arreglo1.o arreglo1.asm 
;ld -o arreglo1 arreglo1.o

section .data
    arreglo db 3,2,0,7,5 
    LEN_arreglo equ $-arreglo

section .bss
    numero resb 1


section .text
    global _start

_start: 
    mov esi, arreglo
    mov edi,0

imprimir:
    mov al, [esi]
    add al,'0'
    
    mov [numero],al

    add esi,1
    add edi,1
    
    

    mov eax,4
    mov ebx,1
    mov ecx,numero
    mov edx,1
    int 80h

    cmp edi, LEN_arreglo
    jb imprimir             ; se ejecuta cunado la bandera carries es activa
    
    

salir:
    mov eax ,1
    int 80H
