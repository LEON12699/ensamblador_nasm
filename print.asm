;autor:Israel León
;Tema: impresión por pantalla 

;To create executable:

; nasm -f elf64 -o first.o first.asm
;ld -o fisrt.o



section .data
    mensaje1 db "Mi nombre es Israel León",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud1 EQU $-mensaje1 ; constante de longitud calcula #caracteres de mensaje

    mensaje2 db "La materia estudiada es lenguaje ensamblador",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud2 EQU $-mensaje2 ; constante de longitud calcula #caracteres de mensaje
    
    mensaje3 db "Mi genero es masculino",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud3 EQU $-mensaje3 ; constante de longitud calcula #caracteres de mensaje
    
section .text
    global _start
_start:


    ;***** imprimir el mensaje *************************
    mov eax, 4 ;        especifica el tipo de subrutina => escritura,salida
    mov ebx, 1 ;        el estandar del tipo de salida 
    mov ecx, mensaje1 ;  el registro ecx se almacena la referencia a imprimir "mensaje"
    mov edx, longitud1;  el registro edx se almacena la referencia a imprimir #caracteres
    int 80H ;           interrupción del sw para linux 

    mov eax, 4;
    mov ebx, 1;
    mov ecx, mensaje2
    mov edx, longitud2
    int 80H

    mov eax, 4;
    mov ebx, 1;
    mov ecx, mensaje3
    mov edx, longitud3
    int 80H

    mov eax,1;          salida de programa, system_exit,sys_exit
    mov ebx,0;          si el retorno 0 == a 200 en la web == OK
    int 80H