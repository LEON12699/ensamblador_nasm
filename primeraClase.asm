;Date: 2020-June-10
;autor:Israel León
;Tema: Impresión por pantalla 

;To create executable:
; el programa es desarrollado bajo nasm

; Using Linux :
; nasm -f elf64 primeraClase.asm
; ld -o primeraCLase primeraClase.o

section .data
    mensaje db "Mi primer programa con NASM",10,;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud EQU $-mensaje ; constante de longitud calcula #caracteres de mensaje

section .text
    global _start
_start:
    ;***** imprimir el mensaje *************************
    mov eax, 4 ;        especifica el tipo de subrutina => escritura,salida
    mov ebx, 1 ;        el estandar del tipo de salida 
    mov ecx, mensaje ;  el registro ecx se almacena la referencia a imprimir "mensaje"
    mov edx, longitud;  el registro edx se almacena la referencia a imprimir #caracteres
    int 80H ;           interrupción del sw para linux 

    mov eax,1;          salida de programa, system_exit,sys_exit
    mov ebx,0;          si el retorno 0 == a 200 en la web == OK
    int 80H

	
