;Date: 2020-August-26
;autor:Israel Leon
;Tema: invertir cadena 
%macro print 2  ; definición de macro con nombre imprimir y recibe dos parámetros
    mov eax,4;		<valor de subrutina para escritura en pantalla>
    mov ebx,1;		estandar para salida de datos
    mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
    mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
    int 80H;			interrupción del sitema	
%endmacro




section .data
    MSG1 db "Leer archivos" ,10
    LEN_MSG1 equ $-MSG1

    MSG2 db "cadena invertida :" ,10
    LEN_MSG2 equ $-MSG2


    MSG3 db "Guardado... en resultado.txt" ,10
    LEN_MSG3 equ $-MSG3


  
    
    NW db  10
    LEN_NW equ $-NW
    
    enviar db "resultado.txt",0 ; change this to yout direction of the file

    archivo db "datos.txt" ; change this to your direction of the file


    
section .bss
    texto resb 500   
    idarchivo resd 1

    idenviar resd 1
    textos2 resb 500
    le resb 2


section .text
    global _start

_start: 
;*********************** abrir el archivo ***********+++
    mov eax,5   ; servicio para apertura de archivo
    mov ebx,archivo  ; direccion del archivo
    mov ecx,0 ; Modo de acceso 
                ; O-RDONLY 0; el achivo se abre oslo para leer
                ; O-WRONLY 1; EL archivo se abre para escritura
                ; O-RDWR   2: EL archivo se abre para lectura y escritura
                ; 0-CREATE 256: Crea el archivo en caso de no existir 
                ; O-APPEND 200h: El archivo se abre solo escritura al final (agregacion del elementos)
    mov edx,777h
    int 80H

    test eax,eax   ;Instrucción de comparación realiza la operación lógica &&  de dos operandos
                    ; peor NO afecta a ninguno de ellos, SOLO afecta al registro de estado.Admite
                    ; todoos los tipos de direccionamiento excepto los dos operandos en memoria
                        ;TEST REG, REG
                        ;TEST REG,MEM
                        ;TEST MEM, REG
                        ;TEST REG,INMEDIATO
                        ;TEST MEM,INMEDIATO

    jz salir
    mov dword [idarchivo],eax
    print MSG1,LEN_MSG1
; ********** leer archivo******
    mov eax,3
    mov ebx,[idarchivo]
    mov ecx, texto
    mov edx,100 ; espacio de memoria
    int 80h

    print texto,500
;*******+cerrar archivo*************

    mov eax,6
    mov ebx,[idarchivo]
    mov ecx,0
    mov edx,0
    int 80h

    mov eax,texto

len:                ; instrucción que calcula la longitud del texto ingresado
    mov ebx, eax    ; se lleva la cadena de eax a ebx

    nextchar:
        cmp byte[ebx], 0 ; cheque si ebx == a 0 
        jz endLen        ; salta a endlen si es asi 
        inc ebx          ; incremeneta ebx en 1
        jmp nextchar     ; continua el ciclo
    endLen:
        sub ebx, eax    ; resta ebx =ebx-eax , da como resultado la longitud en ebx


    mov [le],ebx
    mov ecx,0
    mov ebx,[le]

li:
    mov al,[texto+ecx]
    push ax
    inc ecx
    
    cmp ecx,ebx
    jnz li
    
    mov ecx,0

l2:
    pop ax
    mov [textos2+ecx],al    
    inc ecx
    cmp ecx,ebx
    jnz l2




print NW,LEN_NW
print MSG2,LEN_MSG2
print textos2,[le]
print NW,LEN_NW


    mov eax,8   ; servicio para creare archivos
    mov ebx,enviar  ; direccion del archivo
    mov ecx,1 ; Modo de acceso 
                ; O-RDONLY 0; el achivo se abre oslo para leer
                ; O-WRONLY 1; EL archivo se abre para escritura
                ; O-RDWR   2: EL archivo se abre para lectura y escritura
                ; 0-CREATE 256: Crea el archivo en caso de no existir 
                ; O-APPEND 200h: El archivo se abre solo escritura al final (agregacion del elementos)
    mov edx,777h
    int 80H

    test eax,eax   ;Instrucción de comparación realiza la operación lógica &&  de dos operandos
                    ; peor NO afecta a ninguno de ellos, SOLO afecta al registro de estado.Admite
                    ; todoos los tipos de direccionamiento excepto los dos operandos en memoria
                        ;TEST REG, REG
                        ;TEST REG,MEM
                        ;TEST MEM, REG
                        ;TEST REG,INMEDIATO
                        ;TEST MEM,INMEDIATO

    jz salir
    mov dword [idenviar],eax
    print MSG3,LEN_MSG3
        

;************************+escritura en el archivo *********************
    mov eax,4;		
    mov ebx,[idenviar];		
    mov ecx,textos2;		
    mov edx,[le];		
    int 80H;		







salir:
    mov eax ,1
    int 80H
