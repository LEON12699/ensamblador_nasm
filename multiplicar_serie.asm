;Date: 2020-August-05
;autor:Israel Leon
;Tema: tabla de multiplicar Call  multiplicar 

;do ejecutable linux:
 
;nasm -f elf64 -o multiplicar_call.o multiplicar_call.asm 
;ld -o multiplicar_call multiplicar_call.o

imprimir_x:  
    mov eax,4;		
    mov ebx,1;				
    int 80H;			
ret





section .data
    msg1 db "*" ,0
    LEN_msg1 equ $-msg1

    msg2 db " = " ,0
    LEN_msg2 equ $-msg2

    NW db  10
    LEN_NW equ $-NW
 
section .bss
    a resw 2
    b resw 2
    c resw 2
 
section .text
    global _start

_start: 
    


    mov al, 1


    add al ,'0'
    mov [a],al
    mov ecx,1
    
ciclo:
    push rax
    push rcx
    
    
    mov eax,[a]

    sub eax,'0'
    
    mul ecx

    add eax,'0'



    mov [c],eax


    add ecx,'0'
    mov [b],ecx
    
    

    
    
    mov ecx, a
    mov edx, 2


    call imprimir_x
    mov ecx , msg1
    mov edx , LEN_msg1
    call imprimir_x

    mov ecx, b 
    mov edx,2
    call imprimir_x
    
    mov ecx , msg2
    mov edx , LEN_msg2


    call imprimir_x

    mov ecx , c
    mov edx , 2    

    call imprimir_x


    mov ecx , NW
    mov edx , LEN_NW    

    call imprimir_x

    
    
    pop rcx
    pop rax
    inc ecx
    cmp ecx, 10
    jnz ciclo

    inc rax
    mov [a],rax
    cmp eax, 57
    mov ecx, 1
    jnz ciclo
    



    mov eax ,1
    int 80H
