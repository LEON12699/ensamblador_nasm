;ISRAEL LEON
;SUMA DE NUMEROS ESTÁTICOS

;do ejecutable linux:
 
;nasm -f elf64 -o SUMA.o SUMA.asm 
;ld -o SUMA SUMA.o

%macro imprimir 2  ; definición de macro con nombre imprimir y recibe dos parámetros
	mov eax,4;		valor de subrutina para escritura en pantalla
	mov ebx,1;		estandar para salida de datos
	mov ecx,%1;		primer valor en el macro es el mensaje que se va a imprimir, almacena en registro ecx 
	mov edx,%2;		el segundo valor que define la longitud  del mensaje a imprimir, almacena en registro edx
	int 80H;		interrupcion del sitema	
%endmacro



section .data

    mensaje db 10,"la suma es ",0
    len_mensaje equ $-mensaje

section .bss
    suma resb 5

section .text
    global _start

_start:
;***************imprimir mensaje**********
    
    mov eax,2           ; eax = 2
    mov ebx,4;            ebx=4

    add eax,ebx;        eax= eax+ebx
    add eax,'0';        eax= eax+48 ; lo hacemos para sumar el valor ascci de 0 al resultado y nos entrega la cadena correcta
    mov [suma],eax      ;store suma = eax
    
    imprimir mensaje,len_mensaje ; impresion de mensaje
    imprimir suma,5;        impresion de resultado
    

    mov eax,1;          salida dell sistema
    int 80h