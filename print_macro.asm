;autor:Israel León
;Tema: Ejemplo de macro 

;To create executable:
; con nasm puro:
; nasm -f elf64 -o first.o first.asm
;ld -o fisrt.o

%macro imprime 2
        mov edx,%1;     primer valor en el macro es la longitud que se pone en edx
        mov ecx,%2;     segundo valor es el valor a imprimir "no directo"
        mov ebx,1;      estandar para salida de datos      
        mov eax,4;      valor de subrutina para escritura en pantalla
        int 80h;        interrupcion del sistema linux
%endmacro


section .data
    mensaje1 db "Mi nombre es Israel León",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud1 EQU $-mensaje1 ; constante de longitud calcula #caracteres de mensaje

    mensaje2 db "La materia estudiada es lenguaje ensamblador",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud2 EQU $-mensaje2 ; constante de longitud calcula #caracteres de mensaje
    
    mensaje3 db "Mi genero es masculino",10 ;10 == salto de linea , constante mensaje de 1 byte en memoria
    longitud3 EQU $-mensaje3 ; constante de longitud calcula #caracteres de mensaje
    
section .text
    global _start
_start:

   ; mov eax,'2'
    ;add eax,'0''

    

    imprime longitud1, mensaje1
    imprime longitud2, mensaje2
    imprime longitud3, mensaje3

    ;mov edx, longitud1
    ;mov ecx, mensaje1

;lea:
    ;mov ebx,1
    ;mov eax,4
    ;int 80h



    mov eax,1;          salida de programa, system_exit,sys_exit
    mov ebx,0;          si el retorno 0 == a 200 en la web == OK
    int 80H